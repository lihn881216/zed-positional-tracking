# ZED 物件追蹤

## 相依版本
pyzed >= 3.4 & numpy & OpenGL

### 執行物件追蹤

``` shell
python ZED_object.py
```

### 修改追蹤類別

請於ZED_object.py 內找出第55行中
```python
obj_runtime_param.object_class_filter = [sl.OBJECT_CLASS.PERSON]    # Only detect Persons
```
後面的sl.OBJECT_CLASS.PERSON, 改成你所需要使用的類別項[請參閱](https://www.stereolabs.com/docs/api/python/classpyzed_1_1sl_1_1OBJECT__CLASS.html)
